README
========

This repo is a use case for Spring Boot.

How to use
-------------

~~~
mvn package
~~~

~~~
java -jar usecase-0.0.1.jar
~~~

Then navigate to http://localhost:8080/use-case/swagger-ui.html#/ for APIs documentation.

How to use CSV processing
-----------------
You should define the necessary properties to use CSV file processing. The properties file is the following:  
~~~
src/main/resources/csv.properties
~~~

An example of the properties file is as follows:

~~~
csv.directory=C:\\Users\\birben\\Documents\\Personal\\springboot\\csvs\\
csv.fractions_file=fractions.csv
csv.meter_reading_file=meter_readings.csv
csv.fractions_error_file=fractions_error.csv
csv.meter_reading_error_file=meter_reading_error.csv
~~~

- **csv.directory** is where the all files resides.
- **csv.fractions_file** is the csv file for fractions.
- **csv.meter_reading_file** is the csv file for meter readings.
- **csv.fractions_error_file** is the text file where the errors are logged while adding fractions from fractions file.
- **csv.meter_reading_error_file** is the text file where the errors are logged while adding meter readings from meter readings file.

CSV processing is scheduled to run after 1 hour after previous processing ends.


Assumptions
-------------
- All fractions for each month should be provided.
- All fractions should be sum up to 1.
- A meter reading for a month can not be less than the previous month.
- A meter reading should be consistent with the fractions provided.
- If a fraction is not provided for the month, a meter reading for that month is not acceptable.
- If a connection is invalid, it is neglected but the other connections are processed according to rules.
- The request to add fractions for each profile should be correct. This request is different than the meter readings request and only accepts requests which are completely validated.

Notes
-----------
- You may need to install Lombok plugin to your IDE.
