package com.birben.usecase.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Data
@ToString
@Entity
@Table(name = "fractions", uniqueConstraints = @UniqueConstraint(columnNames = {"profile", "month"}))
public class Fractions implements Serializable{
    @Id
    @GeneratedValue
    private Integer id;

    @Column
    private String profile;

    @Column
    private String month;

    @Column
    private Double fraction;

    protected Fractions() {}

    public Fractions(String profile, String month, Double fraction) {
        this.profile = profile;
        this.month = month;
        this.fraction = fraction;
    }
}
