package com.birben.usecase.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "meter_reading", uniqueConstraints = @UniqueConstraint(columnNames = {"connectionId", "profile", "month"}))
public class MeterReadings implements Serializable{
    // ConnectionID,Profile,Month,Meter reading

    @Id
    @GeneratedValue
    private Integer id;

    @Column
    private String connectionId;

    @Column
    private String profile;

    @Column
    private String month;

    @Column
    private Integer meterReading;

    protected MeterReadings(){}

    public MeterReadings(String connectionId, String profile, String month, Integer meterReading) {
        this.connectionId = connectionId;
        this.profile = profile;
        this.month = month;
        this.meterReading = meterReading;
    }
}
