package com.birben.usecase.batch;

import com.birben.usecase.domain.common.ErrorResponse;
import com.birben.usecase.domain.common.RequestWithValidationResults;
import com.birben.usecase.domain.fractions.FractionCsvLine;
import com.birben.usecase.domain.fractions.FractionRequest;
import com.birben.usecase.domain.fractions.ProfileFractionsRequest;
import com.birben.usecase.domain.meterreading.Connection;
import com.birben.usecase.domain.meterreading.MeterReadingCsvLine;
import com.birben.usecase.domain.meterreading.MeterReadingOfMonth;
import com.birben.usecase.domain.meterreading.MeterReadingRequest;
import com.birben.usecase.service.FractionService;
import com.birben.usecase.service.MeterReadingValidatorService;
import com.birben.usecase.service.MeterReadingsService;
import com.birben.usecase.service.ProfileFractionsValidatorService;
import com.birben.usecase.utils.batch.BatchConfigProperties;
import com.birben.usecase.utils.batch.CSVFileReader;
import com.birben.usecase.utils.enums.MonthEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ScheduledCSVJobs {
    private static final Logger logger = LoggerFactory.getLogger(ScheduledCSVJobs.class);

    @Autowired
    private CSVFileReader csvFileReader;

    @Autowired
    private ProfileFractionsValidatorService profileFractionsValidatorService;

    @Autowired
    private FractionService fractionService;

    @Autowired
    private MeterReadingValidatorService meterReadingValidatorService;

    @Autowired
    private MeterReadingsService meterReadingsService;

    @Autowired
    private BatchConfigProperties batchConfigProperties;

    @Scheduled(fixedDelay = 3600000)
    public void addFractionsFromFile() {
        logger.info("Adding fractions from file");
        boolean exists = Files.exists(Paths.get(batchConfigProperties.getDirectory() + batchConfigProperties.getFractionsFile()));
        if (exists) {
            File file = Paths.get(batchConfigProperties.getDirectory()+batchConfigProperties.getFractionsFile()).toFile();
            List<FractionCsvLine> fractionCsvLines = csvFileReader.loadObjectList(FractionCsvLine.class, file);
            List<ProfileFractionsRequest> profileFractionsRequests = convertToProfileFractionsRequest(fractionCsvLines);
            addProfileFractions(profileFractionsRequests);

        } else {
            logger.error(batchConfigProperties.getFractionsFile() + " does not exist in the directory: " + batchConfigProperties.getDirectory());
        }
    }

    private void addProfileFractions(List<ProfileFractionsRequest> profileFractionsRequests) {
        RequestWithValidationResults<ProfileFractionsRequest> requestRequestWithValidationResults =
                profileFractionsValidatorService.validate(profileFractionsRequests);

        if (!requestRequestWithValidationResults.getErrors().isEmpty()) {
            logErrors(requestRequestWithValidationResults.getErrors(), batchConfigProperties.getFractionsErrorFile());
        }
        if (!requestRequestWithValidationResults.getRequests().isEmpty()){
            fractionService.saveFractions(requestRequestWithValidationResults.getRequests());
            deleteFile(batchConfigProperties.getFractionsFile());
        }
    }

    @Scheduled(fixedDelay = 3600000)
    public void addMeterReadingsFromFile() {
        logger.info("Adding meter readings from file");

        boolean exists = Files.exists(Paths.get(batchConfigProperties.getDirectory() + batchConfigProperties.getMeterReadingFile()));
        if (exists) {
            File file = Paths.get(batchConfigProperties.getDirectory()+batchConfigProperties.getMeterReadingFile()).toFile();
            List<MeterReadingCsvLine> meterReadingCsvLines = csvFileReader.loadObjectList(MeterReadingCsvLine.class, file);
            List<MeterReadingRequest> meterReadingRequests = convertToMeterReadingsRequest(meterReadingCsvLines);
            addMeterReadings(meterReadingRequests);
        } else {
            logger.error(batchConfigProperties.getMeterReadingFile() + " does not exist in the directory: " + batchConfigProperties.getDirectory());
        }
    }

    private void addMeterReadings(List<MeterReadingRequest> meterReadingRequests) {
        RequestWithValidationResults<MeterReadingRequest> requestWithValidationResults =
                meterReadingValidatorService.validate(meterReadingRequests);

        if (!requestWithValidationResults.getErrors().isEmpty()) {
            logErrors(requestWithValidationResults.getErrors(), batchConfigProperties.getMeterReadingErrorFile());
        }
        if (!requestWithValidationResults.getRequests().isEmpty()) {
            meterReadingsService.addMeterReadings(requestWithValidationResults.getRequests());
            deleteFile(batchConfigProperties.getMeterReadingFile());
        }
    }

    private void deleteFile(String filename) {
        try {
            Files.deleteIfExists(Paths.get(batchConfigProperties.getDirectory() + filename));
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }

    }

    private List<MeterReadingRequest> convertToMeterReadingsRequest(List<MeterReadingCsvLine> meterReadingCsvLines) {
        Map<Connection, List<MeterReadingOfMonth>> fractionRequestsConnectionMap = new HashMap<>();

        meterReadingCsvLines.forEach( line -> {
            Connection connection = new Connection(line.getConnectionId(), line.getProfile());
            fractionRequestsConnectionMap.putIfAbsent(connection, new ArrayList<>());
            fractionRequestsConnectionMap.get(connection).add(
                    new MeterReadingOfMonth(MonthEnum.valueOf(line.getMonth()), line.getMeterReading()));

        });

        List<MeterReadingRequest> meterReadingRequests = new ArrayList<>(fractionRequestsConnectionMap.keySet().size());

        for (Map.Entry<Connection, List<MeterReadingOfMonth>> entry : fractionRequestsConnectionMap.entrySet()) {
            MeterReadingRequest meterReadingRequest = new MeterReadingRequest();
            meterReadingRequest.setConnection(entry.getKey());
            meterReadingRequest.setMeterReadings(entry.getValue());

            meterReadingRequests.add(meterReadingRequest);
        }

        return meterReadingRequests;
    }

    private List<ProfileFractionsRequest> convertToProfileFractionsRequest(List<FractionCsvLine> fractionCsvLines) {
        Map<String, List<FractionRequest>> profileFractionListMap = new HashMap<>();

        fractionCsvLines.forEach(line -> {
            profileFractionListMap.putIfAbsent(line.getProfile(), new ArrayList<>());
            profileFractionListMap.get(line.getProfile()).add(new FractionRequest(line.getMonth(), line.getFraction()));
        });

        List<ProfileFractionsRequest> profileFractionsRequests = new ArrayList<>(profileFractionListMap.keySet().size());

        for (Map.Entry<String, List<FractionRequest>> entry : profileFractionListMap.entrySet()) {
            ProfileFractionsRequest request = new ProfileFractionsRequest();
            request.setProfile(entry.getKey());
            request.setFractions(entry.getValue());

            profileFractionsRequests.add(request);
        }

        return profileFractionsRequests;
    }

    private void logErrors(List<ErrorResponse> errors, String filename) {
        logger.info("Logging errors");
        try {

            File file;

            if (!Files.exists(Paths.get(batchConfigProperties.getDirectory() + filename))) {
                file = Files.createFile(Paths.get(batchConfigProperties.getDirectory() + filename)).toFile();
            } else {
                file = Paths.get(batchConfigProperties.getDirectory() + filename).toFile();
            }
            writeErrorsToFile(errors, file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeErrorsToFile(List<ErrorResponse> errors, File file) throws IOException {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {
            bufferedWriter.write(LocalDateTime.now().toString());
            bufferedWriter.newLine();
            bufferedWriter.write("------------------------------------------");
            bufferedWriter.newLine();
            errors.forEach(error -> {
                try {
                    bufferedWriter.write(error.getMessage());
                    bufferedWriter.newLine();
                    bufferedWriter.write(error.getRejectedValue().toString());
                    bufferedWriter.newLine();
                    bufferedWriter.newLine();
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            });
            bufferedWriter.flush();
        }
    }
}
