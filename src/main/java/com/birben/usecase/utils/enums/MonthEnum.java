package com.birben.usecase.utils.enums;

public enum MonthEnum {
    JAN(1, null),
    FEB(2, JAN),
    MAR(3, FEB),
    APR(4, MAR),
    MAY(5, APR),
    JUN(6, MAY),
    JUL(7, JUN),
    AGU(8, JUL),
    SEP(9, AGU),
    OCT(10, SEP),
    NOV(11, OCT),
    DEC(12, NOV);

    private final Integer monthIndex;
    private final MonthEnum previousMonth;

    MonthEnum(final Integer monthIndex, final MonthEnum previousMonth) {
        this.monthIndex = monthIndex;
        this.previousMonth = previousMonth;
    }

    public Integer getMonthIndex() {
        return monthIndex;
    }

    public MonthEnum getPreviousMonth() {
        return previousMonth;
    }
}
