package com.birben.usecase.utils.batch;

import java.io.File;
import java.util.List;

public interface CSVFileReader {
    <T> List<T> loadObjectList(Class<T> type, File file);
}
