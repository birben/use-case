package com.birben.usecase.utils.batch;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:csv.properties")
@ConfigurationProperties(prefix = "csv")
@Data
public class BatchConfigProperties {
    private String directory;
    private String fractionsFile;
    private String meterReadingFile;
    private String fractionsErrorFile;
    private String meterReadingErrorFile;
}