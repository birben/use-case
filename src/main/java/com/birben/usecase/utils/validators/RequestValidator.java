package com.birben.usecase.utils.validators;

import com.birben.usecase.domain.common.ErrorResponse;
import com.birben.usecase.domain.common.Request;
import com.birben.usecase.domain.common.RequestWithValidationResults;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RequestValidator<T extends Request> {

    private List<Validator<T>> validators = new ArrayList<>();

    public void add(Validator<T> validator) {
        validators.add(validator);
    }

    public RequestWithValidationResults<T> validate(List<T> requests) {
        RequestWithValidationResults<T> requestWithValidationResults = new RequestWithValidationResults<>(requests, new ArrayList<>());
        List<ErrorResponse> allErrorResponses = new ArrayList<>();

        for(Validator<T> validator : validators) {
            // If there is no request to validate, give up validating.
            if (!requestWithValidationResults.getRequests().isEmpty()) {
                requestWithValidationResults = validator.validate(requestWithValidationResults.getRequests());
                allErrorResponses.addAll(requestWithValidationResults.getErrors());
            } else {
                break;
            }
        }

        requestWithValidationResults.setErrors(allErrorResponses);

        return requestWithValidationResults;
    }
}
