package com.birben.usecase.utils.validators.meterreading;

import com.birben.usecase.entity.Fractions;
import com.birben.usecase.domain.common.ErrorResponse;
import com.birben.usecase.domain.common.RequestWithValidationResults;
import com.birben.usecase.domain.meterreading.MeterReadingRequest;
import com.birben.usecase.repository.FractionsRepository;
import com.birben.usecase.utils.enums.MonthEnum;
import com.birben.usecase.utils.validators.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Second Validation for Meter Reading.
 * Every connection ID's profile should exists in the db beforehand. We have an assumption for all 12 months should
 * exists. Therefore number of fractions for a profile should be 12.
 */
@Component
public class MeterReadingProfileFractionsValidator implements Validator<MeterReadingRequest> {
    private static final Logger logger = LoggerFactory.getLogger(MeterReadingProfileFractionsValidator.class);

    private static final int FRACTION_COUNT_FOR_A_PROFILE = MonthEnum.values().length;

    private final FractionsRepository fractionsRepository;

    public MeterReadingProfileFractionsValidator(FractionsRepository fractionsRepository) {
        this.fractionsRepository = fractionsRepository;
    }

    @Override
    public RequestWithValidationResults<MeterReadingRequest> validate(List<MeterReadingRequest> requests) {
        RequestWithValidationResults<MeterReadingRequest> requestRequestWithValidationResults = new RequestWithValidationResults<>();

        requests.forEach( request -> {
            List<Fractions> fractions = fractionsRepository.findByProfile(request.getConnection().getProfile());
            if (fractions.size() < FRACTION_COUNT_FOR_A_PROFILE) {
                ErrorResponse errorResponse = new ErrorResponse(request, "Missing fraction value for profile");
                requestRequestWithValidationResults.getErrors().add(errorResponse);
                logger.error("Missing fraction value for profile " + request.getConnection().getProfile());
            } else {
                // validated request.
                requestRequestWithValidationResults.getRequests().add(request);
            }
        });

        return requestRequestWithValidationResults;
    }

}
