package com.birben.usecase.utils.validators;

import com.birben.usecase.domain.common.Request;
import com.birben.usecase.domain.common.RequestWithValidationResults;

import java.util.List;

public interface Validator <T extends Request>{
     RequestWithValidationResults<T> validate(List<T> requests);
}
