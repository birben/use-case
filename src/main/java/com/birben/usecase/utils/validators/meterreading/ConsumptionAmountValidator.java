package com.birben.usecase.utils.validators.meterreading;

import com.birben.usecase.entity.Fractions;
import com.birben.usecase.domain.common.ErrorResponse;
import com.birben.usecase.domain.common.RequestWithValidationResults;
import com.birben.usecase.domain.meterreading.MeterReadingOfMonth;
import com.birben.usecase.domain.meterreading.MeterReadingRequest;
import com.birben.usecase.repository.FractionsRepository;
import com.birben.usecase.utils.enums.MonthEnum;
import com.birben.usecase.utils.validators.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class ConsumptionAmountValidator implements Validator<MeterReadingRequest> {
    private static final Logger logger = LoggerFactory.getLogger(ConsumptionAmountValidator.class);
    private static final Double TOLERANCE_RATIO = 0.25;

    private final FractionsRepository fractionsRepository;

    public ConsumptionAmountValidator(FractionsRepository fractionsRepository) {
        this.fractionsRepository = fractionsRepository;
    }

    @Override
    public RequestWithValidationResults<MeterReadingRequest> validate(List<MeterReadingRequest> requests) {
        RequestWithValidationResults<MeterReadingRequest> requestRequestWithValidationResults =
                new RequestWithValidationResults<>();

        requests.forEach(request -> {
            ErrorResponse response = validate(request);

            if (Objects.nonNull(response)) {
                requestRequestWithValidationResults.getErrors().add(response);
            } else {
                requestRequestWithValidationResults.getRequests().add(request);
            }
        });

        return requestRequestWithValidationResults;
    }

    private ErrorResponse validate(MeterReadingRequest request) {
        List<Fractions> fractions = fractionsRepository.findByProfile(request.getConnection().getProfile());
        List<MeterReadingOfMonth> meterReadingOfMonths = request.getMeterReadings();

        if (meterReadingOfMonths.size() != MonthEnum.values().length) {
            return new ErrorResponse(meterReadingOfMonths, "There should be " + MonthEnum.values().length + " number of meter readings.");
        }

        meterReadingOfMonths.sort(Comparator.comparing(r -> r.getMonth().getMonthIndex()));
        int consumption;
        Map<MonthEnum, Integer> consumptionMap = new HashMap<>(MonthEnum.values().length);

        // Set for the first months consumption
        consumptionMap.put(meterReadingOfMonths.get(0).getMonth(), meterReadingOfMonths.get(0).getMeterReading());

        for (int i = 1 ; i < meterReadingOfMonths.size() ; i++) {
            // Calculate consumptions for each month
            consumption = meterReadingOfMonths.get(i).getMeterReading() - meterReadingOfMonths.get(i-1).getMeterReading();
            consumptionMap.put(meterReadingOfMonths.get(i).getMonth(), consumption);
        }

        return checkForConsumptionRate(consumptionMap, fractions);
    }

    private ErrorResponse checkForConsumptionRate(Map<MonthEnum, Integer> consumptionMap, List<Fractions> fractions) {
        int totalConsumption = consumptionMap.values().stream().mapToInt(Integer::intValue).sum();
        for (Fractions fraction : fractions) {
            if (violateLimits(consumptionMap, totalConsumption, fraction)) {
                logger.error("Consumption of a month should be consistent with " +
                        "the profile values");
                return new ErrorResponse(consumptionMap, "Consumption of a month should be consistent with " +
                        "the profile values");
            }
        }
        return null;
    }

    private boolean violateLimits(Map<MonthEnum, Integer> consumptionMap, int totalConsumption, Fractions fraction) {
        int consumptionOFMonth;
        double expectedConsumption, consumptionTolerance, minAcceptableConsumptionForMonth
                , maxAcceptableConsumptionForMonth;

        expectedConsumption = totalConsumption * fraction.getFraction();
        consumptionOFMonth = consumptionMap.get(MonthEnum.valueOf(fraction.getMonth()));

        consumptionTolerance = consumptionOFMonth * TOLERANCE_RATIO;

        minAcceptableConsumptionForMonth = expectedConsumption - consumptionTolerance;
        maxAcceptableConsumptionForMonth = expectedConsumption + consumptionTolerance;
        if (consumptionOFMonth <= minAcceptableConsumptionForMonth
                || consumptionOFMonth >= maxAcceptableConsumptionForMonth) {
            logger.error(fraction.getMonth() + " violates the consumption rate. consumption: " + consumptionOFMonth
                            + " min acceptable: " + minAcceptableConsumptionForMonth + " max acceptable: "
                            + maxAcceptableConsumptionForMonth);
            return true;
        }
        return false;
    }

}
