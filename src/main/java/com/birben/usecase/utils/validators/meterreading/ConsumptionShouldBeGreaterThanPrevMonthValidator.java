package com.birben.usecase.utils.validators.meterreading;

import com.birben.usecase.domain.common.ErrorResponse;
import com.birben.usecase.domain.common.RequestWithValidationResults;
import com.birben.usecase.domain.meterreading.MeterReadingOfMonth;
import com.birben.usecase.domain.meterreading.MeterReadingRequest;
import com.birben.usecase.utils.validators.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class ConsumptionShouldBeGreaterThanPrevMonthValidator implements Validator<MeterReadingRequest> {
    private static final Logger logger = LoggerFactory.getLogger(ConsumptionShouldBeGreaterThanPrevMonthValidator.class);

    @Override
    public RequestWithValidationResults<MeterReadingRequest> validate(List<MeterReadingRequest> requests) {
        RequestWithValidationResults<MeterReadingRequest> requestRequestWithValidationResults =
                new RequestWithValidationResults<>();
        requests.forEach(request -> {
            ErrorResponse errorResponse = validate(request);
            if (Objects.nonNull(errorResponse)) {
                requestRequestWithValidationResults.getErrors().add(errorResponse);
            } else {
                requestRequestWithValidationResults.getRequests().add(request);
            }
        });
        return requestRequestWithValidationResults;
    }

    private ErrorResponse validate(MeterReadingRequest request) {
        ErrorResponse errorResponse = null;
        List<MeterReadingOfMonth> meterReadings = request.getMeterReadings();
        meterReadings.sort(Comparator.comparing(r -> r.getMonth().getMonthIndex()));

        for (int i = 1 ; i < meterReadings.size() ; i++) {
            // Meter Reading can not be less than the previous month (Validation 1)
            if (meterReadings.get(i).getMeterReading() < meterReadings.get(i-1).getMeterReading()) {
                errorResponse = new ErrorResponse(meterReadings, "Meter Reading can not be less than the previous month");
                logger.error("Meter Reading " + meterReadings.get(i).toString()
                        + " can not be less than the previous month " + meterReadings.get(i-1).toString());
                break;
            }
        }

        return errorResponse;
    }

}
