package com.birben.usecase.utils.validators.fractions;

import com.birben.usecase.domain.common.ErrorResponse;
import com.birben.usecase.domain.common.RequestWithValidationResults;
import com.birben.usecase.domain.fractions.FractionRequest;
import com.birben.usecase.domain.fractions.ProfileFractionsRequest;
import com.birben.usecase.utils.validators.Validator;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProfileFractionsValidator implements Validator<ProfileFractionsRequest>{
    @Override
    public RequestWithValidationResults<ProfileFractionsRequest> validate(List<ProfileFractionsRequest> requests) {
        RequestWithValidationResults<ProfileFractionsRequest> requestWithValidationResults = new RequestWithValidationResults<>();
        requests.forEach( profileFractionsRequest -> {
            double totalFractionsOfProfile =
                    profileFractionsRequest.getFractions().stream().mapToDouble(FractionRequest::getFraction).sum();

            if (totalFractionsOfProfile == 1.0) {
                requestWithValidationResults.getRequests().add(profileFractionsRequest);
            } else {
                requestWithValidationResults.getErrors().add(new ErrorResponse(profileFractionsRequest.getProfile(),
                        "Fractions of each profile should be sum up to 1"));
            }
        });

        return requestWithValidationResults;
    }
}
