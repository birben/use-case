package com.birben.usecase.domain.meterreading;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MeterReadingCsvLine {
    @JsonProperty("ConnectionID")
    private String connectionId;
    @JsonProperty("Profile")
    private String profile;
    @JsonProperty("Month")
    private String month;
    @JsonProperty("Meter reading")
    private Integer meterReading;

}
