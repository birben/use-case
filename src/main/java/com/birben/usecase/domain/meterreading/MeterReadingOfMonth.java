package com.birben.usecase.domain.meterreading;

import com.birben.usecase.utils.enums.MonthEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class MeterReadingOfMonth {
    @JsonProperty(required = true)
    private MonthEnum month;

    @JsonProperty(required = true)
    private Integer meterReading;


}
