package com.birben.usecase.domain.meterreading;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class Connection {
    @JsonProperty(required = true)
    private String connectionId;
    @JsonProperty(required = true)
    private String profile;
}
