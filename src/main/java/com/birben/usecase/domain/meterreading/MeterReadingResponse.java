package com.birben.usecase.domain.meterreading;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MeterReadingResponse {
    private String connectionId;
    private String profile;
    private String month;
    private Integer meterReading;

}
