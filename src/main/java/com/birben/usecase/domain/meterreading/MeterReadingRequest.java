package com.birben.usecase.domain.meterreading;

import com.birben.usecase.domain.common.Request;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class MeterReadingRequest implements Request{
    @JsonProperty(required = true)
    private Connection connection;

    @JsonProperty(required = true)
    private List<MeterReadingOfMonth> meterReadings;

}
