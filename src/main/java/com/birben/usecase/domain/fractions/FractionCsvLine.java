package com.birben.usecase.domain.fractions;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FractionCsvLine {
    @JsonProperty("Month")
    private String month;
    @JsonProperty("Profile")
    private String profile;
    @JsonProperty("Fraction")
    private Double fraction;
}
