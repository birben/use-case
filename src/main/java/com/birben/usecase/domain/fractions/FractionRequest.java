package com.birben.usecase.domain.fractions;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FractionRequest {
    @JsonProperty(required = true)
    private String month;
    @JsonProperty(required = true)
    private Double fraction;
}
