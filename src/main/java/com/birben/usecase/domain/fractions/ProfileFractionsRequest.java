package com.birben.usecase.domain.fractions;

import com.birben.usecase.domain.common.Request;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class ProfileFractionsRequest implements Request {
    @JsonProperty(required = true)
    private String profile;

    private List<FractionRequest> fractions;
}
