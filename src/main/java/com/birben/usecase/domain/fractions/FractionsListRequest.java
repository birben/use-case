package com.birben.usecase.domain.fractions;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class FractionsListRequest {
    @NotNull
    private List<FractionRequest> requests;
}
