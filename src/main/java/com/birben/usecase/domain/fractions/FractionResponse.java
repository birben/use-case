package com.birben.usecase.domain.fractions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FractionResponse {
    private String month;
    private String profile;
    private Double fraction;
}
