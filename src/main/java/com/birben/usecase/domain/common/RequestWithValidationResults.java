package com.birben.usecase.domain.common;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class RequestWithValidationResults<T extends Request> {
    List<T> requests;
    List<ErrorResponse> errors;

    public RequestWithValidationResults() {
        this.requests = new ArrayList<>();
        this.errors = new ArrayList<>();
    }
}
