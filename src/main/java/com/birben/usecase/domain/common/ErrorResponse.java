package com.birben.usecase.domain.common;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorResponse {
    private Object rejectedValue;
    private String message;

}
