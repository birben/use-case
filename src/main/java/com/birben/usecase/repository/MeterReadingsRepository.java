package com.birben.usecase.repository;

import com.birben.usecase.entity.MeterReadings;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MeterReadingsRepository extends CrudRepository<MeterReadings, Integer> {
    List<MeterReadings> findMeterReadingsByConnectionId(String connectionId);

    int deleteMeterReadingsByConnectionId(String connectionId);

    MeterReadings findByConnectionIdAndMonth(String connectionId, String name);
}
