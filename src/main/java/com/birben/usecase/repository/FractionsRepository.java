package com.birben.usecase.repository;

import com.birben.usecase.entity.Fractions;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FractionsRepository extends CrudRepository<Fractions, Integer> {
    List<Fractions> findByProfile(String profile);
    int deleteByProfile(String profile);
}
