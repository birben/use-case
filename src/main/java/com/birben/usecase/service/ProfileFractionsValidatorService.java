package com.birben.usecase.service;

import com.birben.usecase.domain.common.RequestWithValidationResults;
import com.birben.usecase.domain.fractions.ProfileFractionsRequest;

import java.util.List;

public interface ProfileFractionsValidatorService {
    RequestWithValidationResults<ProfileFractionsRequest> validate (List<ProfileFractionsRequest> profileFractionsRequests);
}
