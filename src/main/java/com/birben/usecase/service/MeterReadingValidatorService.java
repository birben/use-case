package com.birben.usecase.service;

import com.birben.usecase.domain.common.RequestWithValidationResults;
import com.birben.usecase.domain.meterreading.MeterReadingRequest;

import java.util.List;

public interface MeterReadingValidatorService {
    RequestWithValidationResults<MeterReadingRequest> validate(List<MeterReadingRequest> meterReadingRequests);
}
