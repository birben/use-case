package com.birben.usecase.service.impl;

import com.birben.usecase.entity.Fractions;
import com.birben.usecase.exceptions.NoFractionsFoundForProfileException;
import com.birben.usecase.domain.common.ErrorResponse;
import com.birben.usecase.domain.fractions.FractionResponse;
import com.birben.usecase.domain.fractions.ProfileFractionsRequest;
import com.birben.usecase.repository.FractionsRepository;
import com.birben.usecase.service.FractionService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Transactional
public class FractionServiceImpl implements FractionService {
    @Autowired
    private FractionsRepository fractionsRepository;

    @Override
    public List<FractionResponse> saveFractions(List<ProfileFractionsRequest> requests) {
        List<FractionResponse> responses = new ArrayList<>();
        requests.forEach(profile -> profile.getFractions().forEach(fractions -> {
            Fractions fraction = new Fractions(profile.getProfile(), fractions.getMonth(),
                    fractions.getFraction());
            saveFractionAndAddResponse(responses, fraction);
        }));
        return responses;
    }

    @Override
    public List<FractionResponse> getFractionsByProfile(String profileName) {
        List<Fractions> fractions = fractionsRepository.findByProfile(profileName);
        List<FractionResponse> fractionResponses = fractions.stream()
                .map(f -> new FractionResponse(f.getProfile(), f.getMonth(), f.getFraction()))
                .collect(Collectors.toList());

        if (Objects.isNull(fractionResponses) || fractionResponses.isEmpty()) {
            throw new NoFractionsFoundForProfileException(new ErrorResponse(profileName, "There is no fractions registered for the given profile" ));
        }

        return fractionResponses;
    }

    @Override
    public List<FractionResponse> updateFractions(List<ProfileFractionsRequest> profileFractionsListRequest) {
        List<FractionResponse> responses = new ArrayList<>();
        profileFractionsListRequest.forEach(profileFractions -> {
            List<FractionResponse> responseList = updateProfileFractions(profileFractions);
            responses.addAll(responseList);
        });

        return responses;
    }

    /**
     * Update Fractions based on profiles. Get all fractions of a profile and based on their months update them.
     * Note: Actually we don't need to check that fractions are not equal, hibernate is smart enough to not update if all
     * fields are the same. We are getting a little burden of the hibernate by that checking.
     * @param profileFractions
     */
    private List<FractionResponse> updateProfileFractions(ProfileFractionsRequest profileFractions) {
        List<FractionResponse> responses = new ArrayList<>();
        List<Fractions> fractionListOfProfile = fractionsRepository.findByProfile(profileFractions.getProfile());
        if (fractionListOfProfile.isEmpty()) {
            throw new NoFractionsFoundForProfileException(new ErrorResponse(profileFractions.getProfile(),
                    "No fractions found for the given profile"));
        }
        fractionListOfProfile.forEach(fraction -> {
            profileFractions
                    .getFractions()
                    .stream()
                    .filter( f -> f.getMonth().equals(fraction.getMonth()) && !Objects.equals(f.getFraction(), fraction.getFraction()))
                    .forEach(f -> {
                        fraction.setFraction(f.getFraction());
                        saveFractionAndAddResponse(responses, fraction);
                    });
        });

        return responses;
    }

    @Override
    public int deleteFractionsOfAProfile(String profileName) {
        return fractionsRepository.deleteByProfile(profileName);
    }

    private void saveFractionAndAddResponse(List<FractionResponse> responses, Fractions f) {
        Fractions savedFraction = fractionsRepository.save(f);
        FractionResponse response = new FractionResponse();
        BeanUtils.copyProperties(savedFraction, response);
        responses.add(response);
    }
}
