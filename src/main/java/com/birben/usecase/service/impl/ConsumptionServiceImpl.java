package com.birben.usecase.service.impl;

import com.birben.usecase.entity.MeterReadings;
import com.birben.usecase.exceptions.NoMeterReadingFound;
import com.birben.usecase.domain.common.ErrorResponse;
import com.birben.usecase.repository.MeterReadingsRepository;
import com.birben.usecase.service.ConsumptionService;
import com.birben.usecase.utils.enums.MonthEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
@Transactional
public class ConsumptionServiceImpl implements ConsumptionService {
    @Autowired
    private MeterReadingsRepository meterReadingsRepository;


    @Override
    public Integer getConsumption(String connectionId, MonthEnum month) {
        MeterReadings meterReadings = meterReadingsRepository.findByConnectionIdAndMonth(connectionId, month.name());
        checkMeterReading(meterReadings, month);
        if (Objects.isNull(month.getPreviousMonth())) {
            return meterReadings.getMeterReading();
        }
        MeterReadings previousMonthMeterReading = meterReadingsRepository.findByConnectionIdAndMonth(connectionId, month.getPreviousMonth().name());
        checkMeterReading(previousMonthMeterReading, month.getPreviousMonth());
        return (meterReadings.getMeterReading() - previousMonthMeterReading.getMeterReading());
    }

    private void checkMeterReading(MeterReadings meterReading, MonthEnum month) {
        if (Objects.isNull(meterReading)) {
            throw new NoMeterReadingFound(new ErrorResponse(null, "No recorded meter reading for month: " + month.name()));
        }
    }
}
