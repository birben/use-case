package com.birben.usecase.service.impl;

import com.birben.usecase.domain.common.RequestWithValidationResults;
import com.birben.usecase.domain.meterreading.MeterReadingRequest;
import com.birben.usecase.repository.FractionsRepository;
import com.birben.usecase.service.MeterReadingValidatorService;
import com.birben.usecase.utils.validators.meterreading.ConsumptionAmountValidator;
import com.birben.usecase.utils.validators.meterreading.ConsumptionShouldBeGreaterThanPrevMonthValidator;
import com.birben.usecase.utils.validators.meterreading.MeterReadingProfileFractionsValidator;
import com.birben.usecase.utils.validators.RequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MeterReadingValidatorServiceImpl implements MeterReadingValidatorService{

    @Autowired
    private FractionsRepository fractionsRepository;

    @Override
    public RequestWithValidationResults<MeterReadingRequest> validate(List<MeterReadingRequest> meterReadingRequests) {
        RequestValidator<MeterReadingRequest> requestValidator = new RequestValidator<>();
        requestValidator.add(new MeterReadingProfileFractionsValidator(fractionsRepository));
        requestValidator.add(new ConsumptionShouldBeGreaterThanPrevMonthValidator());
        requestValidator.add(new ConsumptionAmountValidator(fractionsRepository));

        return requestValidator.validate(meterReadingRequests);
    }
}
