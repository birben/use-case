package com.birben.usecase.service.impl;

import com.birben.usecase.domain.common.RequestWithValidationResults;
import com.birben.usecase.domain.fractions.ProfileFractionsRequest;
import com.birben.usecase.service.ProfileFractionsValidatorService;
import com.birben.usecase.utils.validators.RequestValidator;
import com.birben.usecase.utils.validators.fractions.ProfileFractionsValidator;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProfileFractionsValidatorServiceImpl implements ProfileFractionsValidatorService {
    @Override
    public RequestWithValidationResults<ProfileFractionsRequest> validate(List<ProfileFractionsRequest> profileFractionsRequests) {
        RequestValidator<ProfileFractionsRequest> validator = new RequestValidator<>();
        validator.add(new ProfileFractionsValidator());

        return validator.validate(profileFractionsRequests);
    }
}
