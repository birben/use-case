package com.birben.usecase.service.impl;

import com.birben.usecase.entity.MeterReadings;
import com.birben.usecase.domain.meterreading.MeterReadingOfMonth;
import com.birben.usecase.domain.meterreading.MeterReadingRequest;
import com.birben.usecase.domain.meterreading.MeterReadingResponse;
import com.birben.usecase.repository.MeterReadingsRepository;
import com.birben.usecase.service.MeterReadingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class MeterReadingsServiceImpl implements MeterReadingsService {
    private static final Logger logger = LoggerFactory.getLogger(MeterReadingsServiceImpl.class);

    @Autowired
    private MeterReadingsRepository meterReadingsRepository;

    @Override
    public List<MeterReadingResponse> getMeterReadings(String connectionId) {
        List<MeterReadings> meterReadings = meterReadingsRepository.findMeterReadingsByConnectionId(connectionId);
        return meterReadings.stream()
                .map( f -> new MeterReadingResponse(f.getConnectionId(), f.getProfile(), f.getMonth(), f.getMeterReading()))
                .collect(Collectors.toList());
    }

    @Override
    public List<MeterReadingResponse> addMeterReadings(List<MeterReadingRequest> meterReadingRequests) {
        List<MeterReadingResponse> responses = new ArrayList<>();
        meterReadingRequests.forEach(meterReadingRequest -> {
            String connectionId = meterReadingRequest.getConnection().getConnectionId();
            String profile = meterReadingRequest.getConnection().getProfile();

            meterReadingRequest.getMeterReadings().forEach(request -> {
                MeterReadings meterReadings = new MeterReadings(connectionId, profile
                        , request.getMonth().name(), request.getMeterReading());
                saveMeterReadingAndResponse(responses, meterReadings);
            });

        });

        return responses;
    }

    @Override
    public int deleteMeterReadingsByConnection(String connectionId) {
        return meterReadingsRepository.deleteMeterReadingsByConnectionId(connectionId);
    }

    @Override
    public List<MeterReadingResponse> updateMeterReadings(List<MeterReadingRequest> meterReadingRequests) {
        List<MeterReadingResponse> responses = new ArrayList<>();
        for (MeterReadingRequest request : meterReadingRequests) {
            List<MeterReadingResponse> response = updateMeterReadings(request);
            if (Objects.nonNull(response)) {
                responses.addAll(response);
            }
        }

        return responses;

    }

    private List<MeterReadingResponse> updateMeterReadings(MeterReadingRequest request) {
        Map<String, Integer> meterReadingOfMonth = new HashMap<>();
        List<MeterReadingResponse> responses = new ArrayList<>();

        for (MeterReadingOfMonth reading: request.getMeterReadings()) {
            meterReadingOfMonth.put(reading.getMonth().name(), reading.getMeterReading());
        }

        List<MeterReadings> meterReadingsByConnectionId = meterReadingsRepository.findMeterReadingsByConnectionId(request.getConnection().getConnectionId());

        if (null == meterReadingsByConnectionId || meterReadingsByConnectionId.isEmpty()) {
            logger.error("There is no connection with the specified id");
            return null;
        }

        for (MeterReadings reading : meterReadingsByConnectionId) {
            reading.setMeterReading(meterReadingOfMonth.get(reading.getMonth()));
            saveMeterReadingAndResponse(responses, reading);
        }
        return responses;
    }

    private void saveMeterReadingAndResponse(List<MeterReadingResponse> responses, MeterReadings reading) {
        MeterReadingResponse response = new MeterReadingResponse();
        MeterReadings meterReadings = meterReadingsRepository.save(reading);
        BeanUtils.copyProperties(meterReadings, response);
        responses.add(response);
    }


}
