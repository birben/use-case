package com.birben.usecase.service;

import com.birben.usecase.domain.meterreading.MeterReadingRequest;
import com.birben.usecase.domain.meterreading.MeterReadingResponse;

import java.util.List;

public interface MeterReadingsService {
    List<MeterReadingResponse> getMeterReadings(String connectionId);

    List<MeterReadingResponse> addMeterReadings(List<MeterReadingRequest> meterReadingRequests);

    int deleteMeterReadingsByConnection(String connectionId);

    List<MeterReadingResponse> updateMeterReadings(List<MeterReadingRequest> meterReadingRequests);
}
