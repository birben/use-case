package com.birben.usecase.service;

import com.birben.usecase.domain.fractions.FractionResponse;
import com.birben.usecase.domain.fractions.ProfileFractionsRequest;

import java.util.List;

public interface FractionService {


    List<FractionResponse> saveFractions(List<ProfileFractionsRequest> requests);

    List<FractionResponse> getFractionsByProfile(String profileName);

    List<FractionResponse> updateFractions(List<ProfileFractionsRequest> requests);

    int deleteFractionsOfAProfile(String profileName);
}
