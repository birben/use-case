package com.birben.usecase.service;

import com.birben.usecase.utils.enums.MonthEnum;

public interface ConsumptionService {
Integer getConsumption(String connectionId, MonthEnum month);
}
