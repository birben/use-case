package com.birben.usecase.controller;


import com.birben.usecase.service.ConsumptionService;
import com.birben.usecase.utils.enums.MonthEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("consumption")
@Api(value = "Consumption of Clients")
public class ConsumptionController {

    @Autowired
    private ConsumptionService consumptionService;

    @ApiOperation(value = "Get Consumption", notes = "This api provides the consumption of the requested month based " +
            "on the client. In order to get the consumption of the month, you should have profile fraction " +
            "values and meter readings of the requested client beforehand.")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Consumption can not be evaluated unless meter readings are provided."),
            @ApiResponse(code = 200, message = "Consumption evaluated successfully", response = Integer.class)
    })
    @GetMapping("/{connectionId}/{month}")
    public ResponseEntity<Integer> getConsumption(@PathVariable("connectionId") String connectionId,
                                                 @PathVariable("month")MonthEnum month) {

        return new ResponseEntity<>(consumptionService.getConsumption(connectionId, month), HttpStatus.OK);
    }

}
