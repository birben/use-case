package com.birben.usecase.controller;

import com.birben.usecase.domain.common.ErrorResponse;
import com.birben.usecase.domain.common.RequestWithValidationResults;
import com.birben.usecase.domain.meterreading.MeterReadingRequest;
import com.birben.usecase.domain.meterreading.MeterReadingResponse;
import com.birben.usecase.exceptions.NoMeterReadingFound;
import com.birben.usecase.service.MeterReadingValidatorService;
import com.birben.usecase.service.MeterReadingsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("meter-readings")
@Api(value = "Meter Readings")
public class MeterReadingsController {

    @Autowired
    private MeterReadingsService meterReadingsService;

    @Autowired
    private MeterReadingValidatorService meterReadingValidatorService;

    @ApiOperation(value = "List Meter Readings", nickname = "List Meter Readings", response = MeterReadingResponse.class,
    responseContainer = "List", produces = MediaType.APPLICATION_JSON_VALUE, notes = "This api lists the meter readings of" +
            "the requested client's connection id.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Meter readings of the connection listed successfully."),
            @ApiResponse(code = 404, message = "No meter readings found for the connection id")
    })
    @GetMapping("/{connectionId}")
    public ResponseEntity<List<MeterReadingResponse>> getMeterReadings(@PathVariable("connectionId") String connectionId) {
        List<MeterReadingResponse> meterReadings = meterReadingsService.getMeterReadings(connectionId);
        if (meterReadings.isEmpty()) {
            throw new NoMeterReadingFound(new ErrorResponse(connectionId, "No meter reading found for the given connection id"));
        }
        return new ResponseEntity<>(meterReadings, HttpStatus.OK);
    }

    @ApiOperation(value = "Add meter readings" , nickname = "Add meter readings", consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE, notes = "This api adds the meter readings of client's connections based on months." +
            "Validation rules defined in the description are applied. If a connection is not valid based on the rules," +
            "it is neglected and the other connections are saved.", response = MeterReadingResponse.class, responseContainer = "List")
    @ApiResponses({
            @ApiResponse(code = 201, message = "All connection ids' meter readings are added successfully."),
            @ApiResponse(code = 207, message = "Some of the connection ids are added but some of them failed to meet " +
                    "validation criteria"),
            @ApiResponse(code = 400, message = "All of the connections ids are failed to meet validation criteria")
    })
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RequestWithValidationResults> addMeterReadings(@RequestBody List<MeterReadingRequest> meterReadingRequests) {

        RequestWithValidationResults<MeterReadingRequest> meterReadingWithValidation =
                meterReadingValidatorService.validate(meterReadingRequests);
        List<MeterReadingResponse> meterReadingResponses =
                meterReadingsService.addMeterReadings(meterReadingWithValidation.getRequests());

        return generateMeterReadingResponse(meterReadingWithValidation, meterReadingResponses, false);
    }

    @ApiOperation(value = "Update meter readings", nickname = "Update Meter Readings", consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE, response = MeterReadingResponse.class, responseContainer = "List",
    notes = "This api updates meter readings of requested connection ids. The same validation rules as the add meter readings apply " +
            "to update operation.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "All meter readings of each connection id are updated successfully."),
            @ApiResponse(code = 207, message = "Only validated meter readings of connection ids are updated and " +
                    "invalid ones are neglected."),
            @ApiResponse(code = 400, message = "All meter readings of connection ids are invalid. None of them are updated.")
    })
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RequestWithValidationResults> updateMeterReadings(@RequestBody List<MeterReadingRequest> meterReadingRequests) {
        RequestWithValidationResults<MeterReadingRequest> meterReadingWithValidation =
                meterReadingValidatorService.validate(meterReadingRequests);
        if (meterReadingWithValidation.getRequests().isEmpty()) {
            return generateMeterReadingResponse(meterReadingWithValidation, null, true);
        }
        List<MeterReadingResponse> meterReadingResponses = meterReadingsService.updateMeterReadings(meterReadingWithValidation.getRequests());

        return generateMeterReadingResponse(meterReadingWithValidation, meterReadingResponses, true);
    }

    @ApiOperation(value = "Delete Meter Readings", nickname = "Delete Meter Readings", notes = "This api deletes the meter " +
            "readings of the requested connection id. It returns the total number of deleted meter readings of the connection.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Meter readings of the connection is deleted successfully."),
            @ApiResponse(code = 404, message = "No meter readings found for the given connection id")
    })
    @DeleteMapping("/{connectionId}")
    public ResponseEntity<Integer> deleteMeterReadingsByConnection(@PathVariable("connectionId") String connectionId) {
        int numOfMeterReadingsDeleted =  meterReadingsService.deleteMeterReadingsByConnection(connectionId);
        if (numOfMeterReadingsDeleted == 0 ) {
            return new ResponseEntity<>(numOfMeterReadingsDeleted, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(numOfMeterReadingsDeleted, HttpStatus.OK);
    }

    private ResponseEntity<RequestWithValidationResults> generateMeterReadingResponse(RequestWithValidationResults requestWithValidationResults
            , List<MeterReadingResponse> meterReadingResponses, boolean isUpdate) {
        HttpStatus response;

        if (requestWithValidationResults.getErrors().isEmpty()) {
            if (isUpdate) {
                response = HttpStatus.OK;
            } else {
                response = HttpStatus.CREATED;
            }
        } else if (Objects.isNull(requestWithValidationResults.getRequests()) || requestWithValidationResults.getRequests().isEmpty()) {
            response = HttpStatus.BAD_REQUEST;
        } else {
            response = HttpStatus.MULTI_STATUS;
        }

        return new ResponseEntity<>(requestWithValidationResults, response);
    }
}
