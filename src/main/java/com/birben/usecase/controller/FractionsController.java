package com.birben.usecase.controller;


import com.birben.usecase.domain.common.RequestWithValidationResults;
import com.birben.usecase.domain.fractions.FractionResponse;
import com.birben.usecase.domain.fractions.ProfileFractionsRequest;
import com.birben.usecase.exceptions.InvalidProfileFractionsException;
import com.birben.usecase.service.FractionService;
import com.birben.usecase.service.ProfileFractionsValidatorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("fractions")
@Api(value = "Profile Fractions")
public class FractionsController {

    @Autowired
    private FractionService fractionService;

    @Autowired
    private ProfileFractionsValidatorService profileFractionsValidatorService;

    @ApiOperation(value = "Get Fractions", nickname = "Get Fractions",
    notes = "This service returns all the fractions of a profile.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = FractionResponse.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "No fractions found for the given profile")
    })
    @GetMapping("/{profileName}")
    public List<FractionResponse> getFractions(@PathVariable("profileName") String profileName) {
        return fractionService.getFractionsByProfile(profileName);
    }

    @ApiOperation(value = "Add fractions", nickname = "Add fractions", notes = "This api is used to add fractions of a " +
            "profile. All fractions of the profile must sum up to 1. We assume that all fractions of the months should be provided." +
            "This means that every profile should have 12 fractions. All added fractions are returned as the response.",
            consumes = MediaType.APPLICATION_JSON_VALUE, response = FractionResponse.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Fractions of the profiles are created successfully."),
            @ApiResponse(code = 400, message = "Request object is not able to pass the validation requirements.")
    })
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RequestWithValidationResults> fractions(@RequestBody List<ProfileFractionsRequest>  profileFractionsRequest) {
        RequestWithValidationResults<ProfileFractionsRequest> requestWithValidationResults =
                profileFractionsValidatorService.validate(profileFractionsRequest);
        if (!requestWithValidationResults.getErrors().isEmpty()) {
            throw new InvalidProfileFractionsException(requestWithValidationResults.getErrors());
        }
        List<FractionResponse> fractions = fractionService.saveFractions(requestWithValidationResults.getRequests());
        return generateFractionsResponse(requestWithValidationResults, fractions, false);
    }

    @ApiOperation(value = "Update fractions", nickname = "Update fractions", consumes = MediaType.APPLICATION_JSON_VALUE,
    notes = "This api is used to update fractions. The same validations of add fractions are valid for updating the fraction" +
            "operation.", response = FractionResponse.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Fractions of the profiles are updated successfully."),
            @ApiResponse(code = 400, message = "Request object is not able to pass the validation requirements."),
            @ApiResponse(code = 404, message = "There is no fractions for the profile to update.")
    })
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RequestWithValidationResults> updateFractions(@RequestBody List<ProfileFractionsRequest> profileFractionsRequest) {
        RequestWithValidationResults<ProfileFractionsRequest> requestWithValidationResults =
                profileFractionsValidatorService.validate(profileFractionsRequest);
        if (!requestWithValidationResults.getErrors().isEmpty()) {
            throw new InvalidProfileFractionsException(requestWithValidationResults.getErrors());
        }
        List<FractionResponse> fractions = fractionService.updateFractions(profileFractionsRequest);
        return generateFractionsResponse(requestWithValidationResults, fractions, true);
    }

    @ApiOperation(value = "Delete Fraction of a Profile", nickname = "Delete Fractions",
    notes = "This api deletes the fractions of the requested profile. It returns number of deleted fractions.",
    response = Integer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Fractions of the profile removed successfully."),
            @ApiResponse(code = 404, message = "There is no fractions found for the given profile.")
    })
    @DeleteMapping("/{profileName}")
    public ResponseEntity<Integer> deleteFractionsOfAProfile(@PathVariable("profileName") String profileName) {
        int numOfProfileFractionsDeleted = fractionService.deleteFractionsOfAProfile(profileName);
        if (numOfProfileFractionsDeleted == 0 ) {
            return new ResponseEntity<>(numOfProfileFractionsDeleted, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(numOfProfileFractionsDeleted, HttpStatus.OK);
    }

    private ResponseEntity<RequestWithValidationResults> generateFractionsResponse(RequestWithValidationResults requestWithValidationResults
            , List<FractionResponse> meterReadingResponses, boolean isUpdate) {
        HttpStatus response;

        if (requestWithValidationResults.getErrors().isEmpty()) {
            if (isUpdate) {
                response = HttpStatus.OK;
            } else {
                response = HttpStatus.CREATED;
            }
        } else if (Objects.isNull(requestWithValidationResults.getRequests()) || requestWithValidationResults.getRequests().isEmpty()) {
            response = HttpStatus.BAD_REQUEST;
        } else {
            response = HttpStatus.MULTI_STATUS;
        }

        return new ResponseEntity<>(requestWithValidationResults, response);
    }
}
