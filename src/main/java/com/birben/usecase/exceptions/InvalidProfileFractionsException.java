package com.birben.usecase.exceptions;

import com.birben.usecase.domain.common.ErrorResponse;
import lombok.Getter;

import java.util.List;

public class InvalidProfileFractionsException extends RuntimeException {
    @Getter
    private final List<ErrorResponse> errorResponse;

    public InvalidProfileFractionsException(List<ErrorResponse> response) {
        super();
        this.errorResponse = response;
    }
}
