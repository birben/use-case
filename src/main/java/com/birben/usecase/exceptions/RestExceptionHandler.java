package com.birben.usecase.exceptions;

import com.birben.usecase.domain.common.ErrorResponse;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;

import java.util.List;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ExceptionHandlerExceptionResolver{

    @ExceptionHandler(InvalidProfileFractionsException.class)
    public ResponseEntity<List<ErrorResponse>> handleInvalidFractionsSumException(InvalidProfileFractionsException exception) {
        return new ResponseEntity<>(exception.getErrorResponse(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NoMeterReadingFound.class)
    public ResponseEntity<ErrorResponse> handleNoMeterReadingFound(NoMeterReadingFound exception) {
        return new ResponseEntity<>(exception.getError(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(NoFractionsFoundForProfileException.class)
    public ResponseEntity<ErrorResponse> handleNoFractionsFoundForProfileException(NoFractionsFoundForProfileException exception) {
        return new ResponseEntity<>(exception.getErrors(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleUnhandledExceptions(Exception exception) {
        logger.error(exception.getMessage(), exception);
        ErrorResponse errorResponse = new ErrorResponse(null, "Something went wrong!");
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
