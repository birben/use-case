package com.birben.usecase.exceptions;

import com.birben.usecase.domain.common.ErrorResponse;
import lombok.Getter;

public class NoFractionsFoundForProfileException extends RuntimeException{
    @Getter
    private final ErrorResponse errors;
    public NoFractionsFoundForProfileException(ErrorResponse errors) {
        super(errors.getMessage());
        this.errors = errors;
    }

}
