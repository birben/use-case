package com.birben.usecase.exceptions;

import com.birben.usecase.domain.common.ErrorResponse;
import lombok.Getter;

public class NoMeterReadingFound extends RuntimeException {
    @Getter
    private final ErrorResponse error;

    public NoMeterReadingFound(ErrorResponse error) {
        super(error.getMessage());
        this.error = error;
    }
}
