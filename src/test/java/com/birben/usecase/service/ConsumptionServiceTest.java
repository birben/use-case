package com.birben.usecase.service;

import com.birben.usecase.entity.MeterReadings;
import com.birben.usecase.repository.MeterReadingsRepository;
import com.birben.usecase.service.impl.ConsumptionServiceImpl;
import com.birben.usecase.utils.enums.MonthEnum;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class ConsumptionServiceTest {
    private static final String PROFILE = "Home";
    private static final String JANUARY = MonthEnum.JAN.name();
    private static final String FEBRUARY = MonthEnum.FEB.name();
    private static final String CONNECTION_ID = "0001";
    private static final Integer JANUARY_READING = 10;
    private static final Integer FEBRUARY_READING = 25;

    @TestConfiguration
    static class ConsumptionServiceTestConfiguration {
        @Bean
        public ConsumptionServiceImpl consumptionService() {
            return new ConsumptionServiceImpl();
        }
    }

    @Autowired
    private ConsumptionService consumptionService;

    @MockBean
    private MeterReadingsRepository meterReadingsRepository;

    @Before
    public void setUp() {
        MeterReadings janMeterReading = new MeterReadings(CONNECTION_ID, PROFILE, JANUARY, JANUARY_READING);
        MeterReadings febMeterReading = new MeterReadings(CONNECTION_ID, PROFILE, FEBRUARY, FEBRUARY_READING);

        Mockito.when(meterReadingsRepository.findByConnectionIdAndMonth(CONNECTION_ID, JANUARY))
                .thenReturn(janMeterReading);

        Mockito.when(meterReadingsRepository.findByConnectionIdAndMonth(CONNECTION_ID, FEBRUARY))
                .thenReturn(febMeterReading);
    }

    @Test
    public void calculateConsumption_thenReturnConsumptionOfMonth() {
        Integer janConsumption = consumptionService.getConsumption(CONNECTION_ID, MonthEnum.valueOf(JANUARY));

        assertEquals(JANUARY_READING, janConsumption);

        Integer febConsumption = consumptionService.getConsumption(CONNECTION_ID, MonthEnum.valueOf(FEBRUARY));
        assertEquals(Optional.of((FEBRUARY_READING - JANUARY_READING)).get(), Optional.of(febConsumption).get());
    }
}
