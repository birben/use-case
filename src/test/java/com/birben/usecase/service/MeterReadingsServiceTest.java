package com.birben.usecase.service;

import com.birben.usecase.common.util.MeterReadingUtility;
import com.birben.usecase.entity.MeterReadings;
import com.birben.usecase.domain.meterreading.MeterReadingRequest;
import com.birben.usecase.domain.meterreading.MeterReadingResponse;
import com.birben.usecase.repository.MeterReadingsRepository;
import com.birben.usecase.service.impl.MeterReadingsServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class MeterReadingsServiceTest {
    private static final String CONNECTION_ID = "0001";
    private static final String PROFILE = "Home";
    private static final String MONTH = "JAN";
    private static final Integer METER_READING = 10;

    @TestConfiguration
    static class MeterReadingServiceImplTestContextConfiguration {

        @Bean
        public MeterReadingsServiceImpl meterReadingsService() {
            return new MeterReadingsServiceImpl();
        }
    }

    @Autowired
    private MeterReadingsService meterReadingsService;

    @MockBean
    private MeterReadingsRepository meterReadingsRepository;

    @Before
    public void setUp() {
        List<MeterReadings> meterReadings = new ArrayList<>(1);
        MeterReadings meterReading = new MeterReadings(CONNECTION_ID, PROFILE, MONTH, METER_READING);
        meterReadings.add(meterReading);

        Mockito.when(meterReadingsRepository.findMeterReadingsByConnectionId(CONNECTION_ID))
                .thenReturn(meterReadings);

        Mockito.when(meterReadingsRepository.save(meterReading))
                .thenReturn(meterReading);

        Mockito.when(meterReadingsRepository.deleteMeterReadingsByConnectionId(CONNECTION_ID))
                .thenReturn(1);
    }

    @Test
    public void whenValidClientId_thenMeterReadingsShouldBeFound() {
        List<MeterReadingResponse> meterReadingsList = meterReadingsService.getMeterReadings(CONNECTION_ID);

        assertEquals(1, meterReadingsList.size());
    }

    @Test
    public void whenSaveMeterReadingIsSuccess_thenMeterReadingShouldBeReturned() {
        List<MeterReadingRequest> meterReadingRequests = MeterReadingUtility.generateSingleMeterReadingRequests(CONNECTION_ID, PROFILE, MONTH, METER_READING);

        List<MeterReadingResponse> meterReadingResponses = meterReadingsService.addMeterReadings(meterReadingRequests);

        assertEquals(1, meterReadingResponses.size());
        assertEquals(METER_READING, meterReadingResponses.get(0).getMeterReading());
    }

    @Test
    public void whenDeleteMeterReadingOfConnection_thenNumOfDeletedRowsShouldBeReturned() {
        int numOfReturnedRows = meterReadingsService.deleteMeterReadingsByConnection(CONNECTION_ID);

        assertEquals(1, numOfReturnedRows);
    }

    @Test
    public void whenUpdateMeterReadings_thenOnlyReturnUpdatedOnes() {
        List<MeterReadingRequest> meterReadingRequests = MeterReadingUtility.generateSingleMeterReadingRequests(CONNECTION_ID, PROFILE, MONTH, METER_READING);

        List<MeterReadingResponse> meterReadingResponses = meterReadingsService.updateMeterReadings(meterReadingRequests);

        assertEquals(1, meterReadingResponses.size());
        assertEquals(CONNECTION_ID, meterReadingResponses.get(0).getConnectionId());

    }




}