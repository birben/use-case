package com.birben.usecase.service;

import com.birben.usecase.common.util.FractionUtility;
import com.birben.usecase.entity.Fractions;
import com.birben.usecase.exceptions.NoFractionsFoundForProfileException;
import com.birben.usecase.domain.fractions.FractionResponse;
import com.birben.usecase.domain.fractions.ProfileFractionsRequest;
import com.birben.usecase.repository.FractionsRepository;
import com.birben.usecase.service.impl.FractionServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class FractionServiceTest {
    private static final String PROFILE = "Home";
    private static final String MONTH = "Jan";
    private static final Double FRACTION = 0.2;

    private static final String INVALID_PROFILE = "Work";
    private static final Double FRACTION_TO_UPDATE = 0.3;

    @TestConfiguration
    static class FractionServiceImplTestContextConfiguration {

        @Bean
        public FractionServiceImpl fractionService() {
            return new FractionServiceImpl();
        }

    }

    @Autowired
    private FractionService fractionService;

    @MockBean
    private FractionsRepository fractionsRepository;

    @Before
    public void setUp() {
        Fractions fractions = new Fractions(PROFILE, MONTH, FRACTION);

        List<Fractions> fractionsList = new ArrayList<>(1);
        fractionsList.add(fractions);

        Mockito.when(fractionsRepository.findByProfile(fractions.getProfile()))
                .thenReturn(fractionsList);

        Mockito.when(fractionsRepository.save(fractions))
                .thenReturn(fractions);

        Mockito.when(fractionsRepository.deleteByProfile(PROFILE))
                .thenReturn(1);
    }

    @Test
    public void whenValidProfile_thenReturnFractions() {
        List<FractionResponse> fractionsByProfile = fractionService.getFractionsByProfile(PROFILE);

        assertEquals(1, fractionsByProfile.size());
        assertEquals(FRACTION, fractionsByProfile.get(0).getFraction());
    }

    @Test(expected = NoFractionsFoundForProfileException.class)
    public void whenInValidProfile_thenReturnEmpty() {
        List<FractionResponse> fractionsByProfile = fractionService.getFractionsByProfile(INVALID_PROFILE);
    }

    @Test
    public void whenSaveFractionsSuccess_thenFractionsShouldBeReturned() {
        List<ProfileFractionsRequest> profileFractionsRequests = FractionUtility.generateProfileFractionsListRequest(PROFILE, MONTH, FRACTION);

        List<FractionResponse> fractionResponses = fractionService.saveFractions(profileFractionsRequests);

        assertEquals(1, fractionResponses.size());
        assertEquals(MONTH, fractionResponses.get(0).getMonth());
    }

    @Test
    public void whenUpdateFractionsSuccess_thenFractionsShouldBeReturned() {
        List<ProfileFractionsRequest> profileFractionsRequests = FractionUtility.generateProfileFractionsListRequest(PROFILE, MONTH, FRACTION_TO_UPDATE);

        List<FractionResponse> fractionResponses = fractionService.updateFractions(profileFractionsRequests);

        assertEquals(1, fractionResponses.size());
        assertEquals(MONTH, fractionResponses.get(0).getMonth());
    }

    @Test
    public void whenDeleteSuccess_thenNumOfRowsDeletedReturned() {
        int numOfDeletedRows = fractionService.deleteFractionsOfAProfile(PROFILE);

        assertEquals(1, numOfDeletedRows);
    }


}
