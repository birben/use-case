package com.birben.usecase.repository;

import com.birben.usecase.entity.Fractions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class FractionsRepositoryTest {

    @Autowired
    private FractionsRepository fractionsRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void whenFindByProfile_thenReturnFractions() {
        Fractions fractions = new Fractions("Home", "JAN", 0.1);
        entityManager.persist(fractions);

        List<Fractions> fractionsByProfile = fractionsRepository.findByProfile(fractions.getProfile());

        assertEquals(fractionsByProfile.size(), 1);
        assertEquals(fractionsByProfile.get(0).getMonth(), "JAN");
    }

    @Test
    public void whenDeleteByProfile_thenReturnNumOfRowsDeleted() {
        Fractions fractions = new Fractions("Home", "JAN", 0.1);
        entityManager.persist(fractions);

        int numOfDeletedRows = fractionsRepository.deleteByProfile(fractions.getProfile());

        assertEquals(numOfDeletedRows, 1);
    }
}
