package com.birben.usecase.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.birben.usecase.entity.MeterReadings;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)

@DataJpaTest
public class MeterReadingsRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private MeterReadingsRepository meterReadingsRepository;

    @Test
    public void whenFindByConnectionId_thenReturnMeterReading() {
        MeterReadings meterReadings = new MeterReadings("0001", "Home", "JAN", 10);
        entityManager.persist(meterReadings);

        List<MeterReadings> meterReadingsByConnectionId = meterReadingsRepository.findMeterReadingsByConnectionId(meterReadings.getConnectionId());

        assertNotNull(meterReadingsByConnectionId);
        assertEquals(meterReadingsByConnectionId.size(), 1);
        assertEquals(meterReadingsByConnectionId.get(0).getMonth(), "JAN");
    }

    @Test
    public void whenFindByConnectionIdAndMonth_thenReturnMeterReading() {
        MeterReadings meterReadings = new MeterReadings("0001", "Home", "JAN", 10);
        entityManager.persist(meterReadings);

        MeterReadings meterReadingsByConnectionIdAndMonth = meterReadingsRepository.findByConnectionIdAndMonth(meterReadings.getConnectionId(), meterReadings.getMonth());

        assertNotNull(meterReadingsByConnectionIdAndMonth);
        assertEquals(meterReadingsByConnectionIdAndMonth.getMonth(), "JAN");
    }

    @Test
    public void whenDeleteByConnectionId_thenReturnNumOfRowsDeleted() {
        MeterReadings meterReadings = new MeterReadings("0001", "Home", "JAN", 10);
        entityManager.persist(meterReadings);

        int numOfRowsDeleted = meterReadingsRepository.deleteMeterReadingsByConnectionId(meterReadings.getConnectionId());

        assertEquals(numOfRowsDeleted, 1);
    }

    //     int deleteMeterReadingsByConnectionId(String connectionId);
    // MeterReadings findByConnectionIdAndMonth(String connectionId, String name);
}
