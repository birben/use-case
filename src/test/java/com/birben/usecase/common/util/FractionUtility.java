package com.birben.usecase.common.util;

import com.birben.usecase.domain.fractions.FractionRequest;
import com.birben.usecase.domain.fractions.ProfileFractionsRequest;

import java.util.ArrayList;
import java.util.List;

public class FractionUtility {
    public static List<ProfileFractionsRequest> generateProfileFractionsListRequest(String profile, String month, Double fraction) {
        List<ProfileFractionsRequest> profileFractionsRequests = new ArrayList<>(1);

        ProfileFractionsRequest profileFractionsRequest = new ProfileFractionsRequest();
        profileFractionsRequest.setProfile(profile);

        List<FractionRequest> fractionRequests = new ArrayList<>(1);
        FractionRequest fractionRequest = new FractionRequest();
        fractionRequest.setFraction(fraction);
        fractionRequest.setMonth(month);
        fractionRequests.add(fractionRequest);
        profileFractionsRequest.setFractions(fractionRequests);

        profileFractionsRequests.add(profileFractionsRequest);

        return profileFractionsRequests;
    }
}
