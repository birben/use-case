package com.birben.usecase.common.util;

import com.birben.usecase.domain.meterreading.Connection;
import com.birben.usecase.domain.meterreading.MeterReadingOfMonth;
import com.birben.usecase.domain.meterreading.MeterReadingRequest;
import com.birben.usecase.domain.meterreading.MeterReadingResponse;
import com.birben.usecase.entity.Fractions;
import com.birben.usecase.utils.enums.MonthEnum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MeterReadingUtility {

    private static List<Double> fractionsList = Arrays.asList(0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.05, 0.05, 0.1);

    public static List<MeterReadingRequest> generateMeterReadingRequests(String connectionId, String profile, Integer totalMeterReading) {
        List<MeterReadingRequest> meterReadingRequests = new ArrayList<>(1);
        MeterReadingRequest request = new MeterReadingRequest();
        request.setConnection(new Connection(connectionId, profile));
        request.setMeterReadings(getMeterReadingOfMonth(totalMeterReading));

        meterReadingRequests.add(request);

        return meterReadingRequests;
    }

    private static List<MeterReadingOfMonth> getMeterReadingOfMonth(Integer totalMeterReading) {
        List<MeterReadingOfMonth> meterReadingOfMonths = new ArrayList<>(MonthEnum.values().length);
        int meterReadingCount = 0;
        MeterReadingOfMonth meterReading;
        for (MonthEnum month : MonthEnum.values()) {
            meterReadingCount += totalMeterReading * fractionsList.get(month.getMonthIndex()-1);
            meterReading = new MeterReadingOfMonth(month, meterReadingCount);
            meterReadingOfMonths.add(meterReading);
        }

        return meterReadingOfMonths;
    }

    public static List<Fractions> getFractionsForMeterReadings(String profile) {
        List<Fractions> fractions = new ArrayList<>(MonthEnum.values().length);
        Fractions fraction;
        for (MonthEnum month : MonthEnum.values()) {
            fraction = new Fractions(profile, month.name(), fractionsList.get(month.getMonthIndex()-1));
            fractions.add(fraction);
        }

        return fractions;
    }

    public static List<MeterReadingRequest> generateSingleMeterReadingRequests(String connectionId, String profile, String month, Integer meterReading) {
        List<MeterReadingRequest> meterReadingRequests = new ArrayList<>(1);
        MeterReadingRequest meterReadingRequest = new MeterReadingRequest();
        meterReadingRequest.setConnection(new Connection(connectionId, profile));
        List<MeterReadingOfMonth> meterReadingOfMonths = new ArrayList<>(1);
        MeterReadingOfMonth meterReadingOfMonth = new MeterReadingOfMonth(MonthEnum.valueOf(month),meterReading);
        meterReadingOfMonths.add(meterReadingOfMonth);
        meterReadingRequest.setMeterReadings(meterReadingOfMonths);

        meterReadingRequests.add(meterReadingRequest);
        return meterReadingRequests;
    }

    public static List<MeterReadingResponse> generateMeterReadingResponseBasedOnRequest(List<MeterReadingRequest> mockList) {
        List<MeterReadingResponse> responses = new ArrayList<>(1);
        MeterReadingResponse response = new MeterReadingResponse(mockList.get(0).getConnection().getConnectionId(),
                mockList.get(0).getConnection().getProfile(), mockList.get(0).getMeterReadings().get(0).getMonth().name(),
                mockList.get(0).getMeterReadings().get(0).getMeterReading());

        responses.add(response);

        return responses;
    }
}
