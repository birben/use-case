package com.birben.usecase.controller;

import com.birben.usecase.entity.MeterReadings;
import com.birben.usecase.repository.MeterReadingsRepository;
import com.birben.usecase.service.ConsumptionService;
import com.birben.usecase.utils.enums.MonthEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ConsumptionController.class)
public class ConsumptionControllerTest {

    private static final String CONNECTION_ID = "0001";
    private static final String PROFILE = "Home";
    private static final String JANUARY = MonthEnum.JAN.name();
    private static final String FEBRUARY = MonthEnum.FEB.name();

    private static final Integer JANUARY_METER_READING = 10;
    private static final Integer FEBRUARY_METER_READING = 25;

    private static final Integer JANUARY_CONSUMPTION = 10;
    private static final Integer FEBRUARY_CONSUMPTION = 15;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ConsumptionService service;

    @MockBean
    private MeterReadingsRepository meterReadingsRepository;

    @Test
    public void givenMeterReadings_whenGetConsumption_consumptionOfMonthReturns() throws Exception {
        MeterReadings janMeterReading = new MeterReadings(CONNECTION_ID, PROFILE, JANUARY, JANUARY_METER_READING);
        MeterReadings febMeterReading = new MeterReadings(CONNECTION_ID, PROFILE, FEBRUARY, FEBRUARY_METER_READING);

        given(meterReadingsRepository.findByConnectionIdAndMonth(CONNECTION_ID, JANUARY)).willReturn(janMeterReading);
        given(meterReadingsRepository.findByConnectionIdAndMonth(CONNECTION_ID, FEBRUARY)).willReturn(febMeterReading);

        given(service.getConsumption(CONNECTION_ID, MonthEnum.valueOf(JANUARY))).willReturn(JANUARY_CONSUMPTION);
        given(service.getConsumption(CONNECTION_ID, MonthEnum.valueOf(FEBRUARY))).willReturn(FEBRUARY_CONSUMPTION);

        MvcResult mvcResult = mvc.perform(get("/consumption/" + CONNECTION_ID + "/" + JANUARY)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(java.util.Optional.of(JANUARY_CONSUMPTION).get(), java.util.Optional.of(Integer.parseInt(mvcResult.getResponse().getContentAsString())).get());

    }

}
