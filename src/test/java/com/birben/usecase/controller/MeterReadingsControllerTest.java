package com.birben.usecase.controller;

import com.birben.usecase.common.util.MeterReadingUtility;
import com.birben.usecase.domain.common.RequestWithValidationResults;
import com.birben.usecase.domain.meterreading.MeterReadingRequest;
import com.birben.usecase.domain.meterreading.MeterReadingResponse;
import com.birben.usecase.service.MeterReadingValidatorService;
import com.birben.usecase.service.MeterReadingsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(MeterReadingsController.class)
public class MeterReadingsControllerTest {

    private static final String CONNECTION_ID = "0001";
    private static final String MONTH = "JAN";
    private static final String PROFILE = "Home";
    private static final Integer TOTAL_METER_READING = 120;
    private static final Integer METER_READING = 10;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private MeterReadingsService service;

    @MockBean
    private MeterReadingValidatorService meterReadingValidatorService;

    @Test
    public void givenMeterReadings_whenGetMeterReadings_thenReturnJsonArray() throws Exception {

        MeterReadingResponse meterReadingResponse = new MeterReadingResponse(CONNECTION_ID, PROFILE, MONTH, METER_READING);

        List<MeterReadingResponse> meterReadings = Collections.singletonList(meterReadingResponse);

        given(service.getMeterReadings(CONNECTION_ID)).willReturn(meterReadings);

        mvc.perform(get("/meter-readings/"+CONNECTION_ID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].meterReading", is(meterReadingResponse.getMeterReading())));
    }

    @Test
    public void givenMeterReadings_whenAddMeterReadings_thenReturnAdded() throws Exception {
        // Request to api
        List<MeterReadingRequest> mockList = MeterReadingUtility.generateMeterReadingRequests(CONNECTION_ID, PROFILE, TOTAL_METER_READING);

        // Prepare the response
        List<MeterReadingResponse> meterReadingResponses = MeterReadingUtility.generateMeterReadingResponseBasedOnRequest(mockList);

        // Validated request
        RequestWithValidationResults<MeterReadingRequest> requestWithValidationResults = new RequestWithValidationResults<>();
        requestWithValidationResults.setRequests(mockList);

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String mockMeterReadings = ow.writeValueAsString(mockList);

        given(service.addMeterReadings(Mockito.anyListOf(MeterReadingRequest.class))).willReturn(meterReadingResponses);
        given(meterReadingValidatorService.validate(Mockito.anyListOf(MeterReadingRequest.class)))
                .willReturn(requestWithValidationResults);

        mvc.perform(post("/meter-readings")
                .accept(MediaType.APPLICATION_JSON).content(mockMeterReadings)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.requests[0].meterReadings[0].meterReading", is(meterReadingResponses.get(0).getMeterReading())));

    }

    @Test
    public void givenMeterReadings_whenUpdateMeterReadings_thenReturnAdded() throws Exception {
        // Request to api
        List<MeterReadingRequest> mockList = MeterReadingUtility.generateMeterReadingRequests(CONNECTION_ID, PROFILE, TOTAL_METER_READING);

        // Prepare the response
        List<MeterReadingResponse> meterReadingResponses = MeterReadingUtility.generateMeterReadingResponseBasedOnRequest(mockList);

        // Validated request
        RequestWithValidationResults<MeterReadingRequest> validatedResponse = new RequestWithValidationResults<>();
        validatedResponse.setRequests(mockList);

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String mockMeterReadings = ow.writeValueAsString(mockList);

        given(service.updateMeterReadings(Mockito.anyListOf(MeterReadingRequest.class))).willReturn(meterReadingResponses);
        given(meterReadingValidatorService.validate(Mockito.anyListOf(MeterReadingRequest.class)))
                .willReturn(validatedResponse);

        mvc.perform(put("/meter-readings/")
                .accept(MediaType.APPLICATION_JSON).content(mockMeterReadings)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.requests[0].meterReadings[0].meterReading", is(meterReadingResponses.get(0).getMeterReading())));

    }

    @Test
    public void whenDeleteMeterReadings_thenReturnNumOfDeleted() throws Exception {

        given(service.deleteMeterReadingsByConnection(CONNECTION_ID)).willReturn(1);

        mvc.perform(delete("/meter-readings/"+CONNECTION_ID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void whenDeleteInvalidConnectionId_thenReturnNotFound() throws Exception {

        given(service.deleteMeterReadingsByConnection(CONNECTION_ID)).willReturn(0);

        mvc.perform(delete("/meter-readings/"+CONNECTION_ID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
