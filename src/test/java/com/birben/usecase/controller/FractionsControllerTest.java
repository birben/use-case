package com.birben.usecase.controller;

import com.birben.usecase.common.util.FractionUtility;
import com.birben.usecase.domain.common.RequestWithValidationResults;
import com.birben.usecase.domain.fractions.FractionResponse;
import com.birben.usecase.domain.fractions.ProfileFractionsRequest;
import com.birben.usecase.service.FractionService;
import com.birben.usecase.service.ProfileFractionsValidatorService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyListOf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(FractionsController.class)
public class FractionsControllerTest {

    private static final String MONTH = "JAN";
    private static final String PROFILE = "Home";
    private static final Double FRACTION = 1.0;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private FractionService service;

    @MockBean
    private ProfileFractionsValidatorService profileFractionsValidatorService;

    @Test
    public void givenFractions_whenGetFractions_thenReturnJsonArray() throws Exception {

        FractionResponse fractionResponse = new FractionResponse(PROFILE, MONTH, FRACTION);

        List<FractionResponse> fractions = Collections.singletonList(fractionResponse);

        given(service.getFractionsByProfile(PROFILE)).willReturn(fractions);

        mvc.perform(get("/fractions/"+PROFILE)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].fraction", is(fractionResponse.getFraction())));
    }

    @Test
    public void givenFractions_whenAddFractions_thenReturnAdded() throws Exception {
        FractionResponse fractionResponse = new FractionResponse(PROFILE, MONTH, FRACTION);

        List<FractionResponse> fractions = Collections.singletonList(fractionResponse);

        List<ProfileFractionsRequest> profileFractionsRequests = FractionUtility.generateProfileFractionsListRequest(PROFILE, MONTH, FRACTION);

        RequestWithValidationResults<ProfileFractionsRequest> requestWithValidationResults = new RequestWithValidationResults<>();
        requestWithValidationResults.setRequests(profileFractionsRequests);

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String mockProfileFractions = ow.writeValueAsString(profileFractionsRequests);

        given(service.saveFractions(anyListOf(ProfileFractionsRequest.class))).willReturn(fractions);
        given(profileFractionsValidatorService.validate(anyListOf(ProfileFractionsRequest.class)))
                .willReturn(requestWithValidationResults);

        mvc.perform(post("/fractions")
                .accept(MediaType.APPLICATION_JSON).content(mockProfileFractions)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.requests[0].fractions[0].fraction", is(fractionResponse.getFraction())));

    }

    @Test
    public void givenFractions_whenUpdateFractions_thenReturnAdded() throws Exception {
        FractionResponse fractionResponse = new FractionResponse(PROFILE, MONTH, FRACTION);

        List<FractionResponse> fractions = Collections.singletonList(fractionResponse);

        List<ProfileFractionsRequest> profileFractionsRequests = FractionUtility.generateProfileFractionsListRequest(PROFILE, MONTH, FRACTION);

        RequestWithValidationResults<ProfileFractionsRequest> requestWithValidationResults = new RequestWithValidationResults<>();
        requestWithValidationResults.setRequests(profileFractionsRequests);

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String mockProfileFractions = ow.writeValueAsString(profileFractionsRequests);

        given(service.updateFractions(anyListOf(ProfileFractionsRequest.class))).willReturn(fractions);
        given(profileFractionsValidatorService.validate(anyListOf(ProfileFractionsRequest.class)))
                .willReturn(requestWithValidationResults);

        mvc.perform(put("/fractions")
                .accept(MediaType.APPLICATION_JSON).content(mockProfileFractions)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.requests[0].fractions[0].fraction", is(fractionResponse.getFraction())));

    }

    @Test
    public void whenDeleteFractions_thenReturnNumOfDeleted() throws Exception {

        given(service.deleteFractionsOfAProfile(PROFILE)).willReturn(1);

        mvc.perform(delete("/fractions/"+PROFILE)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void whenDeleteInvalidProfile_thenReturnNotFound() throws Exception {

        given(service.deleteFractionsOfAProfile(PROFILE)).willReturn(0);

        mvc.perform(delete("/fractions/"+PROFILE)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
