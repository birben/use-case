package com.birben.usecase.utils.validators.fractions;

import com.birben.usecase.domain.common.RequestWithValidationResults;
import com.birben.usecase.domain.fractions.FractionRequest;
import com.birben.usecase.domain.fractions.ProfileFractionsRequest;
import com.birben.usecase.utils.enums.MonthEnum;
import com.birben.usecase.utils.validators.Validator;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class ProfileFractionsValidatorTest {
    private final Random r = new Random();

    @Test
    public void whenValidInput_thenSameRequestReturns() {
        Validator<ProfileFractionsRequest> profileFractionsRequestValidator = new ProfileFractionsValidator();

        List<ProfileFractionsRequest> requests = prepareRequests(true);

        RequestWithValidationResults<ProfileFractionsRequest> validatedRequest = profileFractionsRequestValidator.validate(requests);

        assertEquals(requests.size(), validatedRequest.getRequests().size());
        assertThat(validatedRequest.getErrors(), is(empty()));
    }

    @Test
    public void whenInValidInput_thenErrorResponseReturns() {
        Validator<ProfileFractionsRequest> profileFractionsRequestValidator = new ProfileFractionsValidator();

        List<ProfileFractionsRequest> requests = prepareRequests(false);

        RequestWithValidationResults<ProfileFractionsRequest> validatedRequest = profileFractionsRequestValidator.validate(requests);

        assertEquals(requests.size(), validatedRequest.getErrors().size());
        assertThat(validatedRequest.getRequests(), is(empty()));
    }

    private List<ProfileFractionsRequest> prepareRequests(boolean valid) {
        List<ProfileFractionsRequest> requests = new ArrayList<>(3);
        ProfileFractionsRequest profileFractionsRequestForA = prepareProfileFractionsRequest("A", valid);
        ProfileFractionsRequest profileFractionsRequestForB = prepareProfileFractionsRequest("B", valid);
        ProfileFractionsRequest profileFractionsRequestForC = prepareProfileFractionsRequest("C", valid);

        requests.add(profileFractionsRequestForA);
        requests.add(profileFractionsRequestForB);
        requests.add(profileFractionsRequestForC);

        return requests;

    }

    private ProfileFractionsRequest prepareProfileFractionsRequest(String profile, boolean valid) {
        ProfileFractionsRequest profileFractionsRequest = new ProfileFractionsRequest();
        profileFractionsRequest.setProfile(profile);

        List<FractionRequest> fractionRequests = new ArrayList<>(2);
        double fraction = new BigDecimal(r.nextDouble()).setScale(2, RoundingMode.HALF_EVEN).doubleValue();
        FractionRequest janFraction = new FractionRequest(MonthEnum.JAN.name(), fraction);
        FractionRequest febFraction;
        if(valid) {
            febFraction = new FractionRequest(MonthEnum.FEB.name(), 1.0 - fraction);
        } else {
            febFraction = new FractionRequest(MonthEnum.FEB.name(), 1.0 + fraction);
        }

        fractionRequests.add(janFraction);
        fractionRequests.add(febFraction);

        profileFractionsRequest.setFractions(fractionRequests);
        return profileFractionsRequest;
    }

}