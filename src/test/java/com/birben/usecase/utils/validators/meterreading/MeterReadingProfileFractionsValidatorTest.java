package com.birben.usecase.utils.validators.meterreading;

import com.birben.usecase.common.util.MeterReadingUtility;
import com.birben.usecase.domain.common.RequestWithValidationResults;
import com.birben.usecase.domain.meterreading.MeterReadingRequest;
import com.birben.usecase.entity.Fractions;
import com.birben.usecase.repository.FractionsRepository;
import com.birben.usecase.utils.enums.MonthEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
public class MeterReadingProfileFractionsValidatorTest {
    private static final String CONNECTION_ID = "0001";
    private static final String PROFILE = "A";
    private static final Integer TOTAL_METER_READING = 120;

    @MockBean
    private FractionsRepository fractionsRepository;

    @Test
    public void whenValidRequest_thenRequestReturns() {
        MeterReadingProfileFractionsValidator meterReadingProfileFractionsValidator = new MeterReadingProfileFractionsValidator(fractionsRepository);
        List<MeterReadingRequest> requests  = MeterReadingUtility.generateMeterReadingRequests(CONNECTION_ID, PROFILE, TOTAL_METER_READING);
        List<Fractions> fractions = prepareFractions();
        given(fractionsRepository.findByProfile(PROFILE))
                .willReturn(fractions);
        RequestWithValidationResults<MeterReadingRequest> validatedRequest = meterReadingProfileFractionsValidator.validate(requests);

        assertThat(validatedRequest.getErrors(), is(empty()));
        assertThat(validatedRequest.getRequests(), is(requests));

    }

    @Test
    public void whenInvalidRequest_thenErrorsReturn() {
        MeterReadingProfileFractionsValidator meterReadingProfileFractionsValidator = new MeterReadingProfileFractionsValidator(fractionsRepository);
        List<MeterReadingRequest> requests  = MeterReadingUtility.generateMeterReadingRequests(CONNECTION_ID, PROFILE, TOTAL_METER_READING);
        List<Fractions> fractions = Collections.EMPTY_LIST;
        given(fractionsRepository.findByProfile(PROFILE))
                .willReturn(fractions);
        RequestWithValidationResults<MeterReadingRequest> validatedRequest = meterReadingProfileFractionsValidator.validate(requests);

        assertThat(validatedRequest.getErrors(), is(not(empty())));
        assertThat(validatedRequest.getRequests(), is(empty()));

    }

    private List<Fractions> prepareFractions() {
        List<Fractions> fractions = new ArrayList<>();
        Fractions fraction;
        for (MonthEnum month : MonthEnum.values()) {
            fraction = new Fractions(PROFILE, month.name(), 0.1);
            fractions.add(fraction);
        }

        return fractions;
    }

}