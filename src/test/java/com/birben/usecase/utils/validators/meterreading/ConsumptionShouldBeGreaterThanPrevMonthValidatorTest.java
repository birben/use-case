package com.birben.usecase.utils.validators.meterreading;

import com.birben.usecase.domain.common.RequestWithValidationResults;
import com.birben.usecase.domain.meterreading.Connection;
import com.birben.usecase.domain.meterreading.MeterReadingOfMonth;
import com.birben.usecase.domain.meterreading.MeterReadingRequest;
import com.birben.usecase.utils.enums.MonthEnum;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;


public class ConsumptionShouldBeGreaterThanPrevMonthValidatorTest {
    @Test
    public void whenValidInput_thenSameRequestReturns() {
        ConsumptionShouldBeGreaterThanPrevMonthValidator validator = new ConsumptionShouldBeGreaterThanPrevMonthValidator();
        List<MeterReadingRequest> requests = prepareRequests(true);
        RequestWithValidationResults<MeterReadingRequest> validatedRequests = validator.validate(requests);

        assertThat(validatedRequests.getRequests().size(), is(requests.size()));
        assertThat(validatedRequests.getErrors(), is(empty()));
    }

    @Test
    public void whenInValidInput_thenErrorResponseReturns() {
        ConsumptionShouldBeGreaterThanPrevMonthValidator validator = new ConsumptionShouldBeGreaterThanPrevMonthValidator();
        List<MeterReadingRequest> requests = prepareRequests(false);
        RequestWithValidationResults<MeterReadingRequest> validatedRequests = validator.validate(requests);

        assertThat(validatedRequests.getErrors().size(), is(requests.size()));
        assertThat(validatedRequests.getRequests(), is(empty()));
    }

    private List<MeterReadingRequest> prepareRequests(boolean valid) {
        List<MeterReadingRequest> requests = new ArrayList<>();

        MeterReadingRequest requestClient1 = prepareMeterReadingRequest("0001", "A", valid);
        MeterReadingRequest requestClient2 = prepareMeterReadingRequest("0002", "B", valid);

        requests.add(requestClient1);
        requests.add(requestClient2);

        return requests;
    }

    private MeterReadingRequest prepareMeterReadingRequest(String connectionId, String profile, boolean valid) {
        MeterReadingRequest request = new MeterReadingRequest();
        Connection connection = new Connection(connectionId, profile);

        List<MeterReadingOfMonth> meterReadingOfMonths = new ArrayList<>();
        int initialReading = 10;
        meterReadingOfMonths.add(new MeterReadingOfMonth(MonthEnum.JAN, initialReading));

        if (valid) {
            meterReadingOfMonths.add(new MeterReadingOfMonth(MonthEnum.FEB, initialReading++));
            meterReadingOfMonths.add(new MeterReadingOfMonth(MonthEnum.MAR, initialReading++));
        } else {
            meterReadingOfMonths.add(new MeterReadingOfMonth(MonthEnum.FEB, initialReading--));
            meterReadingOfMonths.add(new MeterReadingOfMonth(MonthEnum.MAR, initialReading--));
        }

        request.setConnection(connection);
        request.setMeterReadings(meterReadingOfMonths);

        return request;
    }

}