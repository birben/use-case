package com.birben.usecase.utils.validators.meterreading;

import com.birben.usecase.common.util.MeterReadingUtility;
import com.birben.usecase.domain.common.RequestWithValidationResults;
import com.birben.usecase.domain.meterreading.MeterReadingRequest;
import com.birben.usecase.entity.Fractions;
import com.birben.usecase.repository.FractionsRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class ConsumptionAmountValidatorTest {

    private static final Integer TOTAL_METER_READING = 120;
    private static final String PROFILE = "Home";
    private static final String CONNECTION_ID = "0001";
    @MockBean
    private FractionsRepository fractionsRepository;

    @Before
    public void setUp() {
        List<Fractions> fractions = MeterReadingUtility.getFractionsForMeterReadings(PROFILE);
        Mockito.when(fractionsRepository.findByProfile(PROFILE))
                .thenReturn(fractions);
    }

    @Test
    public void whenValidRequest_ThenReturnRequests() {
        ConsumptionAmountValidator consumptionAmountValidator = new ConsumptionAmountValidator(fractionsRepository);

        List<MeterReadingRequest> meterReadingRequests = MeterReadingUtility.generateMeterReadingRequests(CONNECTION_ID, PROFILE, TOTAL_METER_READING);

        RequestWithValidationResults<MeterReadingRequest> validatedRequests = consumptionAmountValidator.validate(meterReadingRequests);

        assertThat(validatedRequests.getRequests(), is(meterReadingRequests));
        assertThat(validatedRequests.getErrors(), is(empty()));
    }

    @Test
    public void whenInvalidRequest_ThenReturnErrors() {
        ConsumptionAmountValidator consumptionAmountValidator = new ConsumptionAmountValidator(fractionsRepository);

        List<MeterReadingRequest> meterReadingRequests = MeterReadingUtility.generateMeterReadingRequests(CONNECTION_ID, PROFILE, TOTAL_METER_READING);
        // Spoil the meter reading
        int meterReading = meterReadingRequests.get(0).getMeterReadings().get(0).getMeterReading();
        meterReading = meterReading * 10;
        meterReadingRequests.get(0).getMeterReadings().get(0).setMeterReading(meterReading);

        RequestWithValidationResults<MeterReadingRequest> validatedRequests = consumptionAmountValidator.validate(meterReadingRequests);

        assertThat(validatedRequests.getRequests(), is(empty()));
        assertThat(validatedRequests.getErrors(), is(not(empty())));
    }

}